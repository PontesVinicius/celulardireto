import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { PlansComponent } from './pages/plans/plans.component';
import { UserInfoComponent } from './pages/user-info/user-info.component';
import { Error404PageComponent } from './error404-page/error404-page.component';
import { AuthGuard } from './services/guard/auth.guard';
import { CelularDiretoService } from './services/api/celular-direto.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PlansComponent,
    UserInfoComponent,
    Error404PageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [AuthGuard, CelularDiretoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
