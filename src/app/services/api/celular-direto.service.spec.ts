import { TestBed, inject } from '@angular/core/testing';

import { CelularDiretoService } from './celular-direto.service';

describe('CelularDiretoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CelularDiretoService]
    });
  });

  it('should be created', inject([CelularDiretoService], (service: CelularDiretoService) => {
    expect(service).toBeTruthy();
  }));
});
