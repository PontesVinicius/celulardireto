import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Plataformas, Planos } from './interface'
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CelularDiretoService {

  
  private _urlPlataformas = 'http://private-59658d-celulardireto2017.apiary-mock.com/plataformas';
  
  private _urlPlanos = 'http://private-59658d-celulardireto2017.apiary-mock.com/planos/'
  
  constructor(private http: HttpClient) {
  
  }


  getPlatforms(): Observable<Plataformas[]> {

    return this.http.get<Plataformas[]>(this._urlPlataformas)  
  
  }

  getPlans(sku): Observable<Planos> {
    return this.http.get<Planos>(this._urlPlanos + sku)
  }



}
