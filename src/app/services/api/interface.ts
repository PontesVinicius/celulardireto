export interface Plataformas {
    sku: string,
    plataforma: string,
    descricao: string
}

export interface Planos {
    sku: string,
    franquia: string,
    valor: string,
    ativo: boolean
    aparelho?: AparelhosDef[]
}

export interface AparelhosDef {
    nome: string,
    valor: string,
    numeroParcelas: number,
    valorParcela: any
}


