import { Component, OnInit } from '@angular/core';
import { CelularDiretoService } from '../../services/api/celular-direto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {

  public plans: any = []

  constructor(private router: Router, private apiService: CelularDiretoService) {
    
  }

  ngOnInit() {
    let plan = localStorage.getItem('sku')

    this.apiService.getPlans(plan)
      .subscribe(data => this.plans = data)

  }

}
