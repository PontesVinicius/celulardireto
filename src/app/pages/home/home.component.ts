import { Component, OnInit } from '@angular/core';
import { CelularDiretoService } from '../../services/api/celular-direto.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public plataforma = [];

  constructor(private router: Router, private apiService: CelularDiretoService) { }

  ngOnInit() {
     this.apiService.getPlatforms()
        .subscribe(data => this.plataforma = data)
    
    
  }
  replaceText(text: string) {
    let textAdjust = text.replace("|"," ")
    return textAdjust
  }

  saveOnLocalStorage(sku) {
    localStorage.setItem('sku', sku)
    this.router.navigate(['plans'])
  }
}
