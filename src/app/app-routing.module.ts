import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { PlansComponent } from './pages/plans/plans.component';
import { UserInfoComponent } from './pages/user-info/user-info.component';
import { Error404PageComponent } from './error404-page/error404-page.component';
import { AuthGuard } from './services/guard/auth.guard';


const routes: Routes = [

  { path: 'home', component: HomeComponent },

  { path: 'plans', component: PlansComponent, canActivate: [AuthGuard] },

  { path: 'user-info', component: UserInfoComponent, canActivate: [AuthGuard] },

  { path: '', redirectTo: '/home', pathMatch: 'full' },

  { path: '**', component: Error404PageComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
